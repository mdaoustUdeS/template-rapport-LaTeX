# template-rapport-LaTeX

Template de rapports selon la version 1.7 (2019-08-30) du guide de rédaction technique du département de génie électrique et génie informatique de l'université de Sherbrooke.

Vous pouvez suivre les instructions de l'extention [LaTeX-Workshop](https://github.com/James-Yu/LaTeX-Workshop) dans Visual Studio Code pour commencer à travailler facilement avec LaTeX.